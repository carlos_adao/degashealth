/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testes;

import Controller.AuxiliarMedicoController;
import Controller.EnfermeiroController;
import Controller.EquipamentoController;
import Controller.EquipamentoProcedimentoController;
import Controller.EspcMedicaController;
import Controller.MaterialController;
import Controller.MaterialProcedimentoController;
import Controller.MedicoController;
import Controller.ProcedimentoController;
import java.sql.SQLException;

/**
 *
 * @author kaiqu
 */
public class Facade {

    public Facade() {
    }

    /*Manipulações da classe dos médicos*/
    public String novoMedico(String nome, String sexo, String crm, String nacionalidade, String data_nasc, String data_admiss, String data_formatura) throws SQLException {
        MedicoController mc = new MedicoController();
        String r = mc.novoMedico(nome, sexo, crm, nacionalidade, data_nasc, data_admiss, data_formatura);
        return r;
    }

    public String alteraMedico(String crm, String tipo, String novo_parametro) {
        MedicoController mc = new MedicoController();
        return mc.editMedico(crm, tipo, novo_parametro);
    }

    public String encontraMedico(String nome) {
        MedicoController mc = new MedicoController();
        return mc.retornaMedico(nome);
    }

    /*Manipulações da classe dos enfermeiros*/
    public String novoEnfermeiro(String nome, String sexo, String coren, String nacionalidade, String data_nasc, String data_admiss, String data_formatura) throws SQLException {
        EnfermeiroController efc = new EnfermeiroController();
        String r = efc.novoEnfermeiro(nome, sexo, coren, nacionalidade, data_nasc, data_admiss, data_formatura);
        return r;
    }

    public String alteraEnfermeiro(String coren, String tipo, String novo_parametro) {
        EnfermeiroController efc = new EnfermeiroController();
        return efc.editEnfermeiro(coren, tipo, novo_parametro);
    }

    public String encontraEnfermeiro(String nome) {
        EnfermeiroController efc = new EnfermeiroController();
        return efc.retornaEnfermeiro(nome);
    }

    /*Manipulações da classe dos auxiliares medico [tec enfermagem aux de enfermagem]*/
    public String novoAuxiliar(String nome, String sexo, String coren, String nacionalidade, String data_nasc, String data_admiss, String data_formatura) throws SQLException {
        AuxiliarMedicoController amc = new AuxiliarMedicoController();
        String r = amc.novoAuxiliarMedico(nome, sexo, coren, nacionalidade, data_nasc, data_admiss, data_formatura, "te");//'te' cadastra o novo auxiliar como tecnico em enfermagem 'ae' cadastra como auxiliar em enfermagem
        return r;
    }

    public String alteraAuxiliar(String coren, String tipo, String novo_parametro) {
        AuxiliarMedicoController amc = new AuxiliarMedicoController();
        return amc.editAuxiliarMedico(coren, tipo, novo_parametro);

    }

    public String encontraAuxiliar(String nome) {
        AuxiliarMedicoController amc = new AuxiliarMedicoController();
        return amc.retornaAuxiliarMedico(nome);
    }

    /*Manipulações da classe dos procedimentos*/
    public String novoProcedimento(String cod, String desc, String custo) throws SQLException {
        ProcedimentoController pc = new ProcedimentoController();
        String r = pc.novoProcedimento(cod, desc, custo);
        return r;
    }

    public String alteraProcedimento(String cod, String tipo, String novo_parametro) {
        ProcedimentoController pc = new ProcedimentoController();
        return pc.editProcedimento(cod, tipo, novo_parametro);

    }

    public String encontraProcedimento(String cod) {
        ProcedimentoController pc = new ProcedimentoController();
        return pc.retornaPocedimento(cod);
    }

    /*Manipulações da classe das Especialisações Medicas*/
    public String novaEspecialidade(String cod, String desc) throws SQLException {
        EspcMedicaController emc = new EspcMedicaController();
        String r = emc.novaEspecialidade(cod, desc);
        return r;
    }

    public String encontraEspecialidade(String cod) {
        EspcMedicaController emc = new EspcMedicaController();
        return emc.retornaEspecMedica(cod);
    }
    /*Manipulações da classe das Equipamento*/

    public String novoEquipamento(String cod, String desc, String valor) throws SQLException {
        EquipamentoController ec = new EquipamentoController();
        return ec.novoEquipamento(cod, desc, cod);
    }

    public String tombaEquipamento(String cod, String tombo) {
        EquipamentoController ec = new EquipamentoController();
        return ec.tombaEquipamento(cod, tombo);
    }

    /*Manipulações da classe das Materiais*/
    public String novoMaterial(String cod, String desc, String qtd) throws SQLException {
        MaterialController mac = new MaterialController();
        return mac.novoMaterial(cod, desc, qtd);
    }
    /*Manipulações do relacionamento entre as  classes das Materiais e procedimentos*/

    public String materialProcedimento(String cod_proc, String cod_mat) throws SQLException {
        MaterialProcedimentoController mapc = new MaterialProcedimentoController();
        return mapc.novoRelacionamento(cod_proc, cod_mat);
    }
     public String listMateriaisProcedimento(String cod_proc) throws SQLException {
        MaterialProcedimentoController mapc = new MaterialProcedimentoController();
        return mapc.listMateriaisProcedimento(cod_proc);
    }
    
    
    /*Manipulações do relacionamento entre as  classes das equipamento e procedimentos*/
    public String equipamentoProcedimento(String cod_proc, String cod_equi) throws SQLException {
        EquipamentoProcedimentoController eqpc = new EquipamentoProcedimentoController();
        return eqpc.novoRelacionamento(cod_proc, cod_equi);
    }
     public String listEquipamentosProcedimento(String cod_proc) throws SQLException {
        EquipamentoProcedimentoController eqpc = new EquipamentoProcedimentoController();
        return eqpc.listEquipamentosProcedimento(cod_proc);
    }
}
