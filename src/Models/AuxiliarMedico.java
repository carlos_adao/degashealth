/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * Objeto da classe
 */
public class AuxiliarMedico {
    String COREN, Nome, Nacionalidade, Sexo, data_nasc, data_admiss, data_formatura, tipo;// A variável tipo define sé o profissional é um técnico ou um auxiliar de enfermagem

    public AuxiliarMedico(String COREN, String Nome, String Nacionalidade, String Sexo, String data_nasc, String data_admiss, String data_formatura, String tipo) {
        this.COREN = COREN;
        this.Nome = Nome;
        this.Nacionalidade = Nacionalidade;
        this.Sexo = Sexo;
        this.data_nasc = data_nasc;
        this.data_admiss = data_admiss;
        this.data_formatura = data_formatura;
        this.tipo = tipo;
    }

    public String getCOREN() {
        return COREN;
    }

    public void setCOREN(String COREN) {
        this.COREN = COREN;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getNacionalidade() {
        return Nacionalidade;
    }

    public void setNacionalidade(String Nacionalidade) {
        this.Nacionalidade = Nacionalidade;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    public String getData_nasc() {
        return data_nasc;
    }

    public void setData_nasc(String data_nasc) {
        this.data_nasc = data_nasc;
    }

    public String getData_admiss() {
        return data_admiss;
    }

    public void setData_admiss(String data_admiss) {
        this.data_admiss = data_admiss;
    }

    public String getData_formatura() {
        return data_formatura;
    }

    public void setData_formatura(String data_formatura) {
        this.data_formatura = data_formatura;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "AuxiliarMedico{" + "COREN=" + COREN + ", Nome=" + Nome + ", Nacionalidade=" + Nacionalidade + ", Sexo=" + Sexo + ", data_nasc=" + data_nasc + ", data_admiss=" + data_admiss + ", data_formatura=" + data_formatura + ", tipo=" + tipo + '}';
    }


}
