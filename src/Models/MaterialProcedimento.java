/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * 
 */
public class MaterialProcedimento {
    String cod_procedimento, cod_material;

    public MaterialProcedimento(String cod_procedimento, String cod_material) {
        this.cod_procedimento = cod_procedimento;
        this.cod_material = cod_material;
    }

    public String getCod_procedimento() {
        return cod_procedimento;
    }

    public void setCod_procedimento(String cod_procedimento) {
        this.cod_procedimento = cod_procedimento;
    }

    public String getCod_material() {
        return cod_material;
    }

    public void setCod_material(String cod_material) {
        this.cod_material = cod_material;
    }

    @Override
    public String toString() {
        return "MaterialProcedimento{" + "cod_procedimento=" + cod_procedimento + ", cod_material=" + cod_material + '}';
    }
    
}
