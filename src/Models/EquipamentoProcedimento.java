/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * 
 */
public class EquipamentoProcedimento {
     String cod_procedimento, cod_equi;

    public EquipamentoProcedimento(String cod_procedimento, String cod_equi) {
        this.cod_procedimento = cod_procedimento;
        this.cod_equi = cod_equi;
    }

    public String getCod_procedimento() {
        return cod_procedimento;
    }

    public void setCod_procedimento(String cod_procedimento) {
        this.cod_procedimento = cod_procedimento;
    }

    public String getCod_equi() {
        return cod_equi;
    }

    public void setCod_equi(String cod_equi) {
        this.cod_equi = cod_equi;
    }

    @Override
    public String toString() {
        return "EquipamentoProcedimento{" + "cod_procedimento=" + cod_procedimento + ", cod_equi=" + cod_equi + '}';
    }

}
