package Models;

/**
 *
 * 
 */

public class Medico {
    String CRM, Nome, Nacionalidade;
    String Sexo; 
    String data_nasc, data_admiss, data_formatura;

    public Medico(String CRM, String Nome, String Nacionalidade, String Sexo, String data_nasc, String data_admiss, String data_formatura) {
        this.CRM = CRM;
        this.Nome = Nome;
        this.Nacionalidade = Nacionalidade;
        this.Sexo = Sexo;
        this.data_nasc = data_nasc;
        this.data_admiss = data_admiss;
        this.data_formatura = data_formatura;
    }

    public String getCRM() {
        return CRM;
    }

    public void setCRM(String CRM) {
        this.CRM = CRM;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getNacionalidade() {
        return Nacionalidade;
    }

    public void setNacionalidade(String Nacionalidade) {
        this.Nacionalidade = Nacionalidade;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    public String getData_nasc() {
        return data_nasc;
    }

    public void setData_nasc(String data_nasc) {
        this.data_nasc = data_nasc;
    }

    public String getData_admiss() {
        return data_admiss;
    }

    public void setData_admiss(String data_admiss) {
        this.data_admiss = data_admiss;
    }

    public String getData_formatura() {
        return data_formatura;
    }

    public void setData_formatura(String data_formatura) {
        this.data_formatura = data_formatura;
    }

    @Override
    public String toString() {
        return "Medico{" + "CRM=" + CRM + ", Nome=" + Nome + ", Nacionalidade=" + Nacionalidade + ", Sexo=" + Sexo + ", data_nasc=" + data_nasc + ", data_admiss=" + data_admiss + ", data_formatura=" + data_formatura + '}';
    }
    
    
  
    
}
