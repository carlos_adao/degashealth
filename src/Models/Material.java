/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Models;

/**
 *
 * 
 */
public class Material {
    String cod, desc;
    int qtd;

    public Material(String cod, String desc, int qtd) {
        this.cod = cod;
        this.desc = desc;
        this.qtd = qtd;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    @Override
    public String toString() {
        return "Material{" + "cod=" + cod + ", desc=" + desc + ", qtd=" + qtd + '}';
    }
    
}
