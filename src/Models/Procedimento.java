package Models;

/**
 *
 *
 */
public class Procedimento {
    String codigo, descricao;
    float custo;

    public Procedimento(String codigo, String descricao, float custo) {
        this.codigo = codigo;
        this.descricao = descricao;
        this.custo = custo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public float getCusto() {
        return custo;
    }

    public void setCusto(float custo) {
        this.custo = custo;
    }

    @Override
    public String toString() {
        return "Procedimento{" + "codigo=" + codigo + ", descricao=" + descricao + ", custo=" + custo + '}';
    }
    
    
}
