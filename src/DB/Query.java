package DB;

import Models.AuxiliarMedico;
import Models.Enfermeiro;
import Models.Equipamento;
import Models.EquipamentoProcedimento;
import Models.EspcMedica;
import Models.Material;
import Models.MaterialProcedimento;
import Models.Medico;
import Models.Procedimento;
import Models.TecEnfermagem;
import com.mysql.jdbc.exceptions.MySQLIntegrityConstraintViolationException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Query {

    java.sql.Statement stm;

    public Query() throws SQLException {
        stm = ConexaoMySQL.getConexaoMySQL().createStatement();
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO MÉDICO-------------------------------------
     */

    /*Insere um médico*/
    public boolean insertMedico(Medico m) {
        try {
            stm.executeUpdate("INSERT INTO medico VALUES ('" + m.getCRM() + "','" + m.getNome() + "','" + m.getNacionalidade() + "','" + m.getSexo() + "','" + m.getData_nasc() + "','" + m.getData_admiss() + "','" + m.getData_formatura() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*Atualiza o nome do médico de acordo com seu CRM*/
    public void updateNomeMedico(String crm, String novo_nome) throws SQLException {
        stm.executeUpdate("UPDATE medico SET nome = '" + novo_nome + "'WHERE crm = '" + crm + "'");
    }

    /*Atualiza o sexo do médico de acordo com seu CRM*/
    public void updateSexoMedico(String crm, String novo_sexo) throws SQLException {
        stm.executeUpdate("UPDATE medico SET sexo = '" + novo_sexo + "'WHERE crm = '" + crm + "'");
    }

    /*Atualiza a nacionalidade do médico de acordo com seu CRM*/
    public void updateNacionalidadeMedico(String crm, String nova_nacionalidade) throws SQLException {
        stm.executeUpdate("UPDATE medico SET nacionalidade = '" + nova_nacionalidade + "'WHERE crm = '" + crm + "'");
    }

    /*Atualiza a data de nascimento do médico de acordo com seu CRM*/
    public void updateDtNascMedico(String crm, String nova_dt_nasc) throws SQLException {
        stm.executeUpdate("UPDATE medico SET data_nasc = '" + nova_dt_nasc + "' WHERE crm = '" + crm + "'");

    }

    /*Atualiza a data de admissão do médico de acordo com seu CRM*/
    public void updateDtAdmissMedico(String crm, String nova_dt_admss) throws SQLException {
        stm.executeUpdate("UPDATE medico SET data_admiss = '" + nova_dt_admss + "' WHERE crm = '" + crm + "'");
    }

    /*Atualiza a data de admissão do médico de acordo com seu CRM*/
    public void updateDtFormaturaMedico(String crm, String nova_dt_form) throws SQLException {
        stm.executeUpdate("UPDATE medico SET  data_formatura = '" + nova_dt_form + "' WHERE crm = '" + crm + "'");
    }

    /*Exclui um médico de acordo com seu CRM*/
    public void deleteMedico(Medico m) throws SQLException {
        stm.executeUpdate("DELETE FROM medico WHERE crm = '" + m.getCRM() + "'");
    }

    /*Retorna um médico de acordo com seu CRM*/
    public Medico selectMedicoPorCRM(String crm) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from medico where crm = '" + crm + "'")) {
            while (rs.next()) {
                return setMedico(rs);
            }
        }
        return null;
    }

    /*Retorna um médico de acordo com seu nome ou CRM*/
    public Medico selectMedicoPorNome(String nome) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from medico where nome = '" + nome + "'OR CRM = '" + nome + "'")) {
            while (rs.next()) {
                return setMedico(rs);
            }
        }
        return null;
    }

    /*Retorna a data de formatura do medico de acordo com seu CRM*/
    public String selectDataFormaturaMedicoPorCRM(String crm) {
        try (ResultSet rs = stm.executeQuery("Select data_formatura from medico where crm = '" + crm + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de nascimento do medico de acordo com seu CRM*/

    public String selectDataNascimentoMedicoPorCRM(String crm) {
        try (ResultSet rs = stm.executeQuery("Select data_nasc from medico where crm = '" + crm + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de admissão do medico de acordo com seu CRM*/

    public String selectDataAdmissMedicoPorCRM(String crm) {
        try (ResultSet rs = stm.executeQuery("Select data_admiss from medico where crm = '" + crm + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Medico setMedico(ResultSet rs) throws SQLException {
        Medico m;
        String CRM = rs.getString(1);
        String Nome = rs.getString(2);
        String Nacionalidade = rs.getString(3);
        String Sexo = rs.getString(4);
        String data_nasc = rs.getString(5);
        String data_admiss = rs.getString(6);
        String data_formatura = rs.getString(7);

        return new Medico(CRM, Nome, Nacionalidade, Sexo, data_nasc, data_admiss, data_formatura);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO ENFERMEIRO-------------------------------------
     */
    /*Insere um Enfermeiro*/
    public boolean insertEnfermeiro(Enfermeiro e) {
        try {
            stm.executeUpdate("INSERT INTO enfermeiro VALUES ('" + e.getCOREN() + "','" + e.getNome() + "','" + e.getNacionalidade() + "','" + e.getSexo() + "','" + e.getData_nasc() + "','" + e.getData_admiss() + "','" + e.getData_formatura() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    /*Atualiza as informações de um Enfermeiro de acordo com seu COREN*/

    public void updateEnfermeiro(Enfermeiro e) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET coren = '" + e.getCOREN() + "', nome = '" + e.getNome() + "', nacionalidade = '" + e.getNacionalidade() + "', sexo = '" + e.getSexo() + "', data_nasc = '" + e.getData_nasc() + "', data_admiss = '" + e.getData_admiss() + "', data_formatura  ='" + e.getData_formatura() + "' WHERE coren = '" + e.getCOREN() + "'");
    }
    /*Atualiza o nome do enfermeiro de acordo com seu COREN*/

    public void updateNomeEnfermeiro(String coren, String novo_nome) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET nome = '" + novo_nome + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza o sexo do enfermeiro de acordo com seu COREN*/

    public void updateSexoEnfermeiro(String coren, String novo_sexo) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET sexo = '" + novo_sexo + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza a nacionalidade do enfermeiro de acordo com seu COREN*/

    public void updateNacionalidadeEnfermeiro(String coren, String nova_nacionalidade) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET nacionalidade = '" + nova_nacionalidade + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza a data de nascimento do enfermeiro de acordo com seu COREN*/

    public void updateDtNascEnfermeiro(String coren, String nova_dt_nasc) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET data_nasc = '" + nova_dt_nasc + "' WHERE coren = '" + coren + "'");
    }
    /*Atualiza a data de admissão do enfermeiro de acordo com seu COREN*/

    public void updateDtAdmissEnfermeiro(String coren, String nova_dt_admss) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET data_admiss = '" + nova_dt_admss + "' WHERE coren = '" + coren + "'");
    }

    /*Atualiza a data de admissão do enfermeiro de acordo com seu COREN*/
    public void updateDtFormaturaEnfermeiro(String coren, String nova_dt_form) throws SQLException {
        stm.executeUpdate("UPDATE enfermeiro SET  data_formatura = '" + nova_dt_form + "' WHERE coren = '" + coren + "'");
    }

    /*Exclui um Enfermeiro de acordo com seu COREN*/
    public void deleteEnfermeiro(Enfermeiro e) throws SQLException {
        stm.executeUpdate("DELETE FROM enfermeiro WHERE coren = '" + e.getCOREN() + "'");
    }
    /*Retorna um Enfermeiro de acordo com seu COREN*/

    public Enfermeiro selectEnfermeiroCOREN(String coren) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from enfermeiro where coren = '" + coren + "'")) {
            while (rs.next()) {
                return setEnfermeiro(rs);
            }
        }
        return null;
    }
    /*Retorna um Enfermeiro de acordo com seu nome*/

    public Enfermeiro selectEnfermeiroPorNome(String nome) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from enfermeiro where nome = '" + nome + "'OR coren = '" + nome + "'")) {
            while (rs.next()) {
                return setEnfermeiro(rs);
            }
        }
        return null;
    }
    /*Retorna a data de formatura do enfermeiro de acordo com seu COREN*/

    public String selectDataFormaturaEnfermeiroPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_formatura from enfermeiro where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de nascimento do enfermeiro de acordo com seu COREN*/

    public String selectDataNascimentoEnfermeiroPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_nasc from enfermeiro where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de admissão do enfermeiro de acordo com seu CRM*/

    public String selectDataAdmissEnfermeiroPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_admiss from enfermeiro where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*Cria um objeto do tipo enfermeiro*/
    public Enfermeiro setEnfermeiro(ResultSet rs) throws SQLException {
        Enfermeiro e;
        String COREN = rs.getString(1);
        String Nome = rs.getString(2);
        String Nacionalidade = rs.getString(3);
        String Sexo = rs.getString(4);
        String data_nasc = rs.getString(5);
        String data_admiss = rs.getString(6);
        String data_formatura = rs.getString(7);

        return new Enfermeiro(COREN, Nome, Nacionalidade, Sexo, data_nasc, data_admiss, data_formatura);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO AUXILIAR MÉDICO-------------------------------------
     */
    /*Insere um Técnico em enfemagem ou um auxiliar em enfermagem*/
    public boolean insertAuxiliarMedico(AuxiliarMedico te) {
        try {
            stm.executeUpdate("INSERT INTO tec_enfermagem VALUES ('" + te.getCOREN() + "','" + te.getNome() + "','" + te.getNacionalidade() + "','" + te.getSexo() + "','" + te.getData_nasc() + "','" + te.getData_admiss() + "','" + te.getData_formatura() + "','" + te.getTipo() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*Atualiza as informações de um Enfermeiro de acordo com seu COREN*/
    public void updateAllInfoAuxMedico(AuxiliarMedico e) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET coren = '" + e.getCOREN() + "', nome = '" + e.getNome() + "', nacionalidade = '" + e.getNacionalidade() + "', sexo = '" + e.getSexo() + "', data_nasc = '" + e.getData_nasc() + "', data_admiss = '" + e.getData_admiss() + "', data_formatura  ='" + e.getData_formatura() + "' WHERE coren = '" + e.getCOREN() + "'");
    }
    /*Atualiza o nome do auxiliar medico de acordo com seu COREN*/

    public void updateNomeAuxMedico(String coren, String novo_nome) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET nome = '" + novo_nome + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza o sexo do auxiliar medico de acordo com seu COREN*/

    public void updateSexoAuxMedico(String coren, String novo_sexo) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET sexo = '" + novo_sexo + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza a nacionalidade do auxiliar medico de acordo com seu COREN*/

    public void updateNacionalidadeAuxMedico(String coren, String nova_nacionalidade) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET nacionalidade = '" + nova_nacionalidade + "'WHERE coren = '" + coren + "'");
    }
    /*Atualiza a data de nascimento do auxiliar de acordo com seu COREN*/

    public void updateDtNascAuxMedico(String coren, String nova_dt_nasc) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET data_nasc = '" + nova_dt_nasc + "' WHERE coren = '" + coren + "'");
    }
    /*Atualiza a data de admissão do auxiliar de acordo com seu COREN*/

    public void updateDtAdmissAuxMedico(String coren, String nova_dt_admss) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET data_admiss = '" + nova_dt_admss + "' WHERE coren = '" + coren + "'");
    }

    /*Atualiza a data de admissão do auxiliar medico de acordo com seu COREN*/
    public void updateDtFormaturaAuxMedico(String coren, String nova_dt_form) throws SQLException {
        stm.executeUpdate("UPDATE tec_enfermagem SET  data_formatura = '" + nova_dt_form + "' WHERE coren = '" + coren + "'");
    }

    /*Exclui um técnico em enfermagem ou auxiliar em enfermagem de acordo com seu COREN*/
    public void deleteAuxiliarMedico(AuxiliarMedico te) throws SQLException {
        stm.executeUpdate("DELETE FROM tec_enfermagem WHERE coren = '" + te.getCOREN() + "'");
    }
    /*Retorna um Técnico em enfermagem ou auxiliar em enfermagem de acordo com seu COREN*/

    public AuxiliarMedico selectAuxiliarMedicoCOREN(String coren, String tipo) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from tec_enfermagem where coren = '" + coren + "' AND tipo ='" + tipo + "' ")) {
            while (rs.next()) {
                return setAuxiliarMedico(rs);
            }
        }
        return null;
    }
    /*Retorna um técnico em enfermagem ou auxiiar em enfermagem de acordo com seu nome*/

    public AuxiliarMedico selectAuxiliarMedicoPorNome(String nome) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from tec_enfermagem where nome = '" + nome + "' OR coren = '" + nome + "'")) {
            while (rs.next()) {
                return setAuxiliarMedico(rs);
            }
        }
        return null;
    }
    /*Retorna a data de formatura do enfermeiro de acordo com seu COREN*/

    public String selectDataFormaturaAuxMedicoPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_formatura  from tec_enfermagem where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de nascimento do enfermeiro de acordo com seu COREN*/

    public String selectDataNascimentoAuxMedicoPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_nasc from tec_enfermagem where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /*Retorna a data de admissão do enfermeiro de acordo com seu CRM*/

    public String selectDataAdmissAuxMedicoPorCOREN(String coren) {
        try (ResultSet rs = stm.executeQuery("Select data_admiss from tec_enfermagem where coren = '" + coren + "'")) {
            while (rs.next()) {
                return rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


    /*Cria um objeto do tipo técnico em enfermagem ou auxiliar em enfermagem*/
    public AuxiliarMedico setAuxiliarMedico(ResultSet rs) throws SQLException {
        AuxiliarMedico te;
        String COREN = rs.getString(1);
        String Nome = rs.getString(2);
        String Nacionalidade = rs.getString(3);
        String Sexo = rs.getString(4);
        String data_nasc = rs.getString(5);
        String data_admiss = rs.getString(6);
        String data_formatura = rs.getString(7);
        String tipo = rs.getString(8);

        return new AuxiliarMedico(COREN, Nome, Nacionalidade, Sexo, data_nasc, data_admiss, data_formatura, tipo);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO PROCEDIMENTOS-------------------------------------
     */
    /*Insere um procedimento na tabela do db*/
    public boolean insertPocedimento(Procedimento pr) {
        try {
            stm.executeUpdate("INSERT INTO procedimento VALUES ('" + pr.getCodigo() + "','" + pr.getDescricao() + "','" + pr.getCusto() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*Atualiza as informações de um Enfermeiro de acordo com seu COREN*/
    public void updateAllInfoProcedimento(Procedimento pr) throws SQLException {
        stm.executeUpdate("UPDATE procedimento SET cod = '" + pr.getCodigo() + "', desc = '" + pr.getDescricao() + "', custo = '" + pr.getCusto() + "'");
    }
    /*Atualiza o descrição do procedimento de acordo com o seu cod*/

    public void updateDescProcedimento(String cod, String nova_desc) throws SQLException {
        stm.executeUpdate("UPDATE procedimento SET descricao = '" + nova_desc + "'WHERE cod = '" + cod + "'");
    }
    /*Atualiza o custo do procedimento de a cordo com seu código*/

    public void updateCustoProcedimento(String cod, String novo_custo) throws SQLException {
        stm.executeUpdate("UPDATE procedimento SET custo = '" + novo_custo + "'WHERE cod = '" + cod + "'");
    }

    /*Retorna um procedimento de acordo com seu código*/
    public Procedimento selectPocedimentoCod(String cod) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from procedimento where cod = '" + cod + "' OR descricao = '" + cod + "'")) {
            while (rs.next()) {
                return setProcedimento(rs);
            }
        }
        return null;
    }

    /*Cria um objeto do tipo procedimento com os dados vindos do banco de dados*/
    public Procedimento setProcedimento(ResultSet rs) throws SQLException {
        Procedimento pr;
        String cod = rs.getString(1);
        String desc = rs.getString(2);
        float custo = Float.parseFloat(rs.getString(3));

        return new Procedimento(cod, desc, custo);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO ESPECIALIDADE MÉDICA-------------------------------------
     */
    /*Insere uma especialidade medica na tabela do db*/
    public boolean insertEspecialidadeMedica(EspcMedica em) {
        try {
            stm.executeUpdate("INSERT INTO espec_medica VALUES ('" + em.getCod() + "','" + em.getDescricao() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    /*Atualiza as epecialisação medica*/

    public void updateDescriEspecMedica(String cod, String nova_desc) throws SQLException {
        stm.executeUpdate("UPDATE espec_medica SET descricao = '" + nova_desc + "'WHERE cod = '" + cod + "'");
    }

    /*Retorna uma especialidade de acordo com seu código*/
    public EspcMedica selectEspecialidadeCod(String cod) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from espec_medica where cod = '" + cod + "' OR descricao = '" + cod + "'")) {
            while (rs.next()) {
                return setEspecMedica(rs);
            }
        }
        return null;
    }

    /*Cria um objeto do tipo especialização medica*/
    public EspcMedica setEspecMedica(ResultSet rs) throws SQLException {
        EspcMedica em;
        String cod = rs.getString(1);
        String desc = rs.getString(2);
        return new EspcMedica(cod, desc);
    }
    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO EQUIPAMENTO-------------------------------------
     */
    /*Insere um equipamento na tabela do db*/

    public boolean insertEquipamento(Equipamento eq) {
        try {
            stm.executeUpdate("INSERT INTO equipamento(cod, descricao, valor) VALUES ('" + eq.getCodigo() + "','" + eq.getDescricao() + "','" + eq.getValor() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    /*Atualiza o tombo do equipamento*/

    public String updatetomboEquipamento(String cod, String tombo) {
        try {
            stm.executeUpdate("UPDATE equipamento SET tombo = '" + tombo + "'WHERE cod = '" + cod + "'");
            return "Equipamento tombado com sucesso";
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return "Erro: Tombo já existente";
        }
    }

    /*Retorna um equipamento de acordo com o seu tombo*/
    public Equipamento selectTomboEquipamentoRepetido(String tombo) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from equipamento where tombo = '" + tombo + "'")) {
            while (rs.next()) {
                return setEquipamento(rs);
            }
        }
        return null;
    }
    /*Cria um objeto do tipo equipamento*/

    public Equipamento setEquipamento(ResultSet rs) throws SQLException {
        Equipamento eq;
        String cod = rs.getString(1);
        String desc = rs.getString(2);
        float valor = Float.parseFloat(rs.getString(3));
        String tombo = rs.getString(4);
        return new Equipamento(cod, desc, valor);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO MATERIAL-------------------------------------
     */
    /*Insere um material na tabela do db*/
    public boolean insertMaterial(Material ma) {
        try {
            stm.executeUpdate("INSERT INTO material(cod, descricao, qtd) VALUES ('" + ma.getCod() + "','" + ma.getDesc() + "','" + ma.getQtd() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*Retorna um material de acordo com seu código*/
    public Material selectMaterialCod(String cod) throws SQLException {
        try (ResultSet rs = stm.executeQuery("Select* from material where cod = '" + cod + "' OR descricao = '" + cod + "'")) {
            while (rs.next()) {
                return setMaterial(rs);
            }
        }
        return null;
    }

    /*Retorna uma lista de materiais de um procedimento*/
    public String selectListaMaterialCodProc(String cod) throws SQLException {
        String saida = "";
        try (ResultSet rs = stm.executeQuery("SELECT material.descricao from material INNER JOIN material_procedimento on material.cod = material_procedimento.cod_mat INNER JOIN procedimento on material_procedimento.cod_proc = procedimento.cod where procedimento.cod = '" + cod + "'")) {
            while (rs.next()) {
                saida = saida + rs.getString(1) + "%";
            }
        }
        return saida;
    }

    /*Cria um objeto do tipo equipamento*/
    public Material setMaterial(ResultSet rs) throws SQLException {
        Material ma;
        String cod = rs.getString(1);
        String desc = rs.getString(2);
        int qtd = Integer.parseInt(rs.getString(3));
        return new Material(cod, desc, qtd);
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO MATERIAL PROCEDIMENTO-------------------------------------
     */
    /*Insere um material-procedimento na tabela do db[relacionamento entre as duas tabelas]*/
    public boolean insertMaterialProcedimento(MaterialProcedimento map) {
        try {
            stm.executeUpdate("INSERT INTO material_procedimento(cod_mat, cod_proc) VALUES ('" + map.getCod_material() + "','" + map.getCod_procedimento() + "')");
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /*
     *----------------------------------- QUERYS USADAS PARA MANIPULAÇÃO DO OBJETO EQUIPAMENTO PROCEDIMENTO-------------------------------------
     */
    /*Insere um equipamento-procedimento na tabela do db[relacionamento entre as duas tabelas]*/
    public boolean insertEquipamentoProcedimento(EquipamentoProcedimento eqp) throws SQLException {
        try {
            stm.executeUpdate("INSERT INTO equipamento_procedimento(cod_equi, cod_proc) VALUES ('" + eqp.getCod_equi() + "','" + eqp.getCod_procedimento() + "')");
            return true;
        } catch (MySQLIntegrityConstraintViolationException ex) {
            Logger.getLogger(Query.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    /*Retorna uma lista de materiais de um procedimento*/
    public String selectListaEquipamentoCodProc(String cod) throws SQLException {
        String saida = "";
        try (ResultSet rs = stm.executeQuery("SELECT equipamento.descricao from equipamento INNER JOIN equipamento_procedimento on equipamento.cod = equipamento_procedimento.cod_equi INNER JOIN procedimento on equipamento_procedimento.cod_proc = procedimento.cod where procedimento.cod = '" + cod + "'")) {
            while (rs.next()) {
                saida = saida + rs.getString(1) + "%";
            }
        }
        return saida;
    }

}
