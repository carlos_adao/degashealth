package Auxiliares;

import java.util.Calendar;

public class Retorna {

    /*Recebe uma data String e retorna o dia int*/
    public static String dia(String d) {
        String[] data = d.split("/");//separo a data pelas barras return 0;
        return data[0];
    }

    /*Recebe uma data String e retorna o dia int*/
    public static String mes(String d) {
        String[] data = d.split("/");//separo a data pelas barras return 0;
        return data[1];
    }

    /*Recebe uma data String e retorna o ano int*/
    public static int ano(String d) {
        String[] data = d.split("/");//separo a data pelas barras return 0;
        return Integer.parseInt(data[2]);
    }

    /*Método usado para retornar a data atual do sistema*/
    public static String dtAtual() {
        Calendar cal = Calendar.getInstance();
        int dia = cal.get(Calendar.DATE);
        int mes = cal.get(Calendar.MONTH) + 1;
        int ano = cal.get(Calendar.YEAR);
        String s_dia, s_mes, s_ano = String.valueOf(ano);

        if (dia < 10) {
            s_dia = "0" + (String.valueOf(dia));
        } else {
            s_dia = String.valueOf(dia);
        }
        if (mes < 10) {
            s_mes = "0" + (String.valueOf(mes));
        } else {
            s_mes = String.valueOf(mes);
        }

        return s_dia + "/" + s_mes + "/" + s_ano;
    }
    
    /*Estabelece como sendo a idade minima para formatura 18 anos*/
    public static String dataMinimaFormatura(String dt_nasc) {
        String data_min = Retorna.dia(dt_nasc) + "/" + Retorna.mes(dt_nasc) + "/" + String.valueOf((Retorna.ano(dt_nasc) + 18));
        return data_min;
    }
}
