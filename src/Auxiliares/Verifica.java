package Auxiliares;

import DB.Query;
import Models.AuxiliarMedico;
import Models.Enfermeiro;
import Models.Equipamento;
import Models.EquipamentoProcedimento;
import Models.EspcMedica;
import Models.Material;
import Models.MaterialProcedimento;
import Models.Medico;
import Models.Procedimento;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Verifica {

    Query query;

    public Verifica() {
        try {
            query = new Query();
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /*
     ************************************MANIPULAÇÕES DA CLASSE DOS MEDICOS******************************************************
     */

    public boolean insereMedico(Medico medico) {
        return query.insertMedico(medico);
    }
    /*Método que verifica se o crm já está cadastrado*/

    public String crmRepetido(String crm) throws SQLException {

        if (query.selectMedicoPorCRM(crm) != null) {
            return "ERRO! CRM Já existente!";
        }
        return null;
    }

    /*Método que retorna um medico de acordo com seu nome*/
    public String getMedicoDB(String nome) {
        Medico medico;
        try {
            medico = query.selectMedicoPorNome(nome);
            if (medico != null) {
                return "" + medico.getNome() + "%" + medico.getSexo() + "%" + medico.getCRM() + "%" + medico.getCRM() + "%" + medico.getData_nasc() + "%" + medico.getData_admiss() + "%" + medico.getData_formatura() + "";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*Método que verifica se o nome do médico já está cadastrado*/
    public static String nomeMedicoRepetido(Query query, String nome) throws SQLException {
        if (query.selectMedicoPorNome(nome) != null) {
            return "Nome já cadastrado";
        }
        return null;
    }

    public String UpdateNomeMedico(String crm, String tipo, String novo_parametro) {
        switch (tipo) {
            case "Nome":
                String saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNomeMedico(crm, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }

            case "Sexo":
                try {
                    query.updateSexoMedico(crm, novo_parametro);
                    return "Alteracao executada com sucesso!";
                } catch (SQLException ex) {
                    Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                    return "Falha na Alteração!";
                }
            case "Nacionalidade":
                saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNacionalidadeMedico(crm, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtNasc":
                saida = dataValidaNascimento(novo_parametro);//verifica se a data de nascimento é valida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtNascMedico(crm, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtAdmiss":

                String dt_form = query.selectDataFormaturaMedicoPorCRM(crm);//pega a data de formatura no médico
                saida = dataAdmissaoValida(dt_form, novo_parametro);//verifica se a data de admissão é válida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtAdmissMedico(crm, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtFormatura":

                String dt_nasc = query.selectDataNascimentoMedicoPorCRM(crm);//pega a data de nascimento do médico
                String dt_admis = query.selectDataAdmissMedicoPorCRM(crm);
                saida = dataFormaturaValida(dt_nasc, novo_parametro, dt_admis);//verifica se a de formatura é valida 
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtFormaturaMedico(crm, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
        }
        return null;
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DOS ENFERMEIROS******************************************************
     */
    public boolean insereEnfermeiro(Enfermeiro enfermeiro) {
        return query.insertEnfermeiro(enfermeiro);
    }

    /*Método que verifica se o coren já está cadastrado [manipulação do painel cadastra enfermeiro]*/
    public String corenRepetido(String crm) {
        try {
            Enfermeiro enfermeiro = query.selectEnfermeiroCOREN(crm);
            if (enfermeiro != null) {
                return "ERRO! COREN Já existente!";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }
    /*Método que retorna um enfermeiro de acordo com seu nome ou coren*/

    public String getEnfermeiroDB(String nome) {
        Enfermeiro enf;
        try {
            enf = query.selectEnfermeiroPorNome(nome);
            if (enf != null) {
                return "" + enf.getNome() + "%" + enf.getSexo() + "%" + enf.getCOREN() + "%" + enf.getNacionalidade() + "%" + enf.getData_nasc() + "%" + enf.getData_admiss() + "%" + enf.getData_formatura() + "";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*Método que verifica se o nome do enfermeiro já está cadastrado*/
    public static String nomeEnfermeiroRepetido(Query query, String nome) throws SQLException {
        if (query.selectEnfermeiroPorNome(nome) != null) {
            return "Nome já cadastrado";
        }
        return "";
    }

    /*Método usado para fazer as atualização de Enfermeiro*/
    public String UpdateEnfermeiro(String coren, String tipo, String novo_parametro) {
        switch (tipo) {
            case "Nome":
                String saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNomeEnfermeiro(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }

            case "Sexo":
                try {
                    query.updateSexoEnfermeiro(coren, novo_parametro);
                    return "Alteracao executada com sucesso!";
                } catch (SQLException ex) {
                    Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                    return "Falha na Alteração!";
                }
            case "Nacionalidade":
                saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNacionalidadeEnfermeiro(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtNasc":
                saida = dataValidaNascimento(novo_parametro);//verifica se a data de nascimento é valida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtNascEnfermeiro(coren, saida);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtAdmiss":
                String dt_form = query.selectDataFormaturaEnfermeiroPorCOREN(coren);//pega a data de formatura do enfermeiro do db
                saida = dataAdmissaoValida(dt_form, novo_parametro);//verifica se a data de admissão é válida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtAdmissEnfermeiro(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtFormatura":
                String dt_nasc = query.selectDataNascimentoEnfermeiroPorCOREN(coren);//pega a data de nascimento do enfermeiro do db
                String dt_admis = query.selectDataAdmissEnfermeiroPorCOREN(coren);//pega a data de adimissão do enfermeiro do db
                saida = dataFormaturaValida(dt_nasc, novo_parametro, dt_admis);//verifica se a de formatura é valida 
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtFormaturaEnfermeiro(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
        }
        return null;
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DOS AUXILIARES MÉDICOS******************************************************
     */
    /*insere um auxiliar médico*/
    public boolean insereAuxiliarMedico(AuxiliarMedico aux_medico) {
        return query.insertAuxiliarMedico(aux_medico);
    }
    /*Método que verifica se o coren já está cadastrado para classe controller auxiliar médico*/

    public String corenRepetidoAuxMedico(String crm) {
        try {
            AuxiliarMedico aux_medico = query.selectAuxiliarMedicoPorNome(crm);
            if (aux_medico != null) {
                return "ERRO! COREN Já existente!";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    /*Método que retorna um auxiliar medico de acordo com seu nome ou coren*/
    public String getAuxiliarMedicoDB(String nome) {
        AuxiliarMedico aux_medico;
        try {
            aux_medico = query.selectAuxiliarMedicoPorNome(nome);
            if (aux_medico != null) {
                return "" + aux_medico.getNome() + "%" + aux_medico.getSexo() + "%" + aux_medico.getCOREN() + "%" + aux_medico.getNacionalidade() + "%" + aux_medico.getData_nasc() + "%" + aux_medico.getData_admiss() + "%" + aux_medico.getData_formatura() + "";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /*Método usado para fazer as atualização de Enfermeiro*/
    public String UpdateAuxiliarMedico(String coren, String tipo, String novo_parametro) {
        switch (tipo) {
            case "Nome":
                String saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNomeAuxMedico(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }

            case "Sexo":
                try {
                    query.updateSexoAuxMedico(coren, novo_parametro);
                    return "Alteracao executada com sucesso!";
                } catch (SQLException ex) {
                    Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                    return "Falha na Alteração!";
                }
            case "Nacionalidade":
                saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateNacionalidadeAuxMedico(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtNasc":
                saida = dataValidaNascimento(novo_parametro);//verifica se a data de nascimento é valida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtNascAuxMedico(coren, saida);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtAdmiss":
                String dt_form = query.selectDataFormaturaAuxMedicoPorCOREN(coren);//pega a data de formatura do enfermeiro do db
                saida = dataAdmissaoValida(dt_form, novo_parametro);//verifica se a data de admissão é válida
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtAdmissAuxMedico(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
            case "DtFormatura":
                String dt_nasc = query.selectDataNascimentoAuxMedicoPorCOREN(coren);//pega a data de nascimento do enfermeiro do db
                String dt_admis = query.selectDataAdmissAuxMedicoPorCOREN(coren);//pega a data de adimissão do enfermeiro do db
                saida = dataFormaturaValida(dt_nasc, novo_parametro, dt_admis);//verifica se a de formatura é valida 
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDtFormaturaAuxMedico(coren, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                        return "Falha na Alteração!";
                    }
                }
        }
        return null;
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DOS PROCEDIMENTOS******************************************************
     */
    public boolean insereProcedimento(Procedimento proc) {
        return query.insertPocedimento(proc);
    }

    /*Método que retorna um procedimento de acordo com o seu código*/
    public String getProcedimentoDB(String cod) {
        Procedimento proc;
        try {
            proc = query.selectPocedimentoCod(cod);
            if (proc != null) {
                return "" + proc.getCodigo() + "%" + proc.getDescricao() + "%" + proc.getCusto();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return "Procedimento não cadastrado!";
        }
        return "Procedimento não cadastrado";
    }
    /*Método usado para fazer as atualização no procedimento*/

    public String UpdateProcedimento(String cod, String tipo, String novo_parametro) {
        switch (tipo) {
            case "Descricao":
                String saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDescProcedimento(cod, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        return "Falha na Alteração!";
                    }
                }
            case "Valor":
                saida = custoValido(novo_parametro);//verifica se o novo custo atende as parêmetros de válidade
                if (saida != null) {
                    return saida;
                }
                try {
                    query.updateCustoProcedimento(cod, novo_parametro);
                    return "Alteracao executada com sucesso!";
                } catch (SQLException ex) {
                    return "Falha na Alteração!";
                }
        }
        return null;
    }

    /*verifica se o codigo do procedimento está repetido*/
    public String codigoProcedimentoRepetido(String cod) {
        try {

            if (query.selectPocedimentoCod(cod) != null) {
                return "Código de Procedimento Ja Cadastrado!";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    public String custoValido(String custo) {

        try {
            float custo_formatado = Float.parseFloat(custo.replace("R$", "").replace("$", "").replace(",", ".").replace(".", ","));
            return null;
        } catch (java.lang.NumberFormatException ex) {
            return "Erro: valor de custo inváido";
        }
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DE ESPECIALIZAÇÃO******************************************************
     */
    public boolean insereEspecMedica(EspcMedica em) {
        return query.insertEspecialidadeMedica(em);
    }

    /*verifica se o codigo da especialidade está repetido*/
    public String codigoEspecialidadeRepetido(String cod) {
        try {
            if (query.selectEspecialidadeCod(cod) != null) {
                return "Código da Especialisação já cadastrado!";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    /*Método que retorna uma especialidade medic de acordo com o seu código*/
    public String getEspecMedicaDB(String cod) {
        EspcMedica espec_medica;
        try {
            espec_medica = query.selectEspecialidadeCod(cod);
            if (espec_medica != null) {
                return "" + espec_medica.getCod() + "%" + espec_medica.getDescricao();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return "Especialisação não cadastrada!";
        }
        return "Especialisação não cadastrada!";
    }

    /*Método usado para fazer as atualização na especialisações médicas */
    public String UpdateEspecMedica(String cod, String tipo, String novo_parametro) {
        switch (tipo) {
            case "Descricao":
                String saida = caracterInvalido(novo_parametro);//verifica se tem caracter invalido na alteração
                if (saida != null) {
                    return saida;
                } else {
                    try {
                        query.updateDescProcedimento(cod, novo_parametro);
                        return "Alteracao executada com sucesso!";
                    } catch (SQLException ex) {
                        return "Falha na Alteração!";
                    }
                }
        }
        return null;
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DE EUIPAMENTOS******************************************************
     */
    public boolean insereEquipamento(Equipamento eq) {
        return query.insertEquipamento(eq);
    }

    /*verifica se o codigo da especialidade está repetido*/
    public String tomboEquipamentoRepetido(String cod) {
        try {
            if (query.selectEspecialidadeCod(cod) != null) {
                return "Erro: Tombo já existente";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    /*Método usado para fazer as atualização no tombo do equipamento */
    public String UpdateTomboEquipamento(String cod, String novo_parametro) {
        return query.updatetomboEquipamento(cod, novo_parametro);
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DE MATERIAIS******************************************************
     */
    public boolean insereMaterial(Material ma) {
        return query.insertMaterial(ma);
    }
    /*verifica se o codigo da especialidade está repetido*/

    public String codMaterialRepetido(String cod) {
        try {
            if (query.selectMaterialCod(cod) != null) {
                return "Erro: Código material já cadastrado!";
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    /*Método que retorna um material de acordo com o seu código*/
    public String getMaterialDB(String cod) {
        Material ma;
        try {
            ma = query.selectMaterialCod(cod);
            if (ma != null) {
                return "" + ma.getCod() + "%" + ma.getDesc() + "%" + ma.getQtd();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return "Material não cadastrado!";
        }
        return "Material não cadastrado!";
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DE RELACIONAMENTO  MATERIAIS - PROCEDIMENTOS******************************************************
     */
    public boolean insereMaterialProcedimento(MaterialProcedimento map) {
        return query.insertMaterialProcedimento(map);
    }

    public String getListaMateriaislDB(String cod) {
        String saida;
        try {
            saida = query.selectListaMaterialCodProc(cod);
            if (!"".equals(saida)) {
                return saida;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return "Não foram encontrados materias para esse procedimento!";
        }
        return "Não foram encontrados materias para esse procedimento!";
    }

    /*
     ************************************MANIPULAÇÕES DA CLASSE DE RELACIONAMENTO  EQUIPAMENTO - PROCEDIMENTOS******************************************************
     */
    public boolean insereEquipamentoProcedimento(EquipamentoProcedimento eqp) {
        try {
            return query.insertEquipamentoProcedimento(eqp);
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String getListaEquipamentoslDB(String cod) {
        String saida;
        try {
            saida = query.selectListaEquipamentoCodProc(cod);
            if (!"".equals(saida)) {
                return saida;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return "Não foram encontrados equipamentos para esse procedimento!";
        }
        return "Não foram encontrados equipamentos para esse procedimento!";
    }

    /*
     ************************************MANIPULAÇÕES DE CARACTERES INVALIDOS - GLOBAL ******************************************************
     */
    public String caracterInvalido(String s) {
        String saida = null;

        for (int i = 0; i < s.length(); i++) {
            char caracter = s.charAt(i);
            switch (caracter) {
                case '%':
                    return "ERRO! Caracter '%' Invalido!";
                case '#':
                    return "ERRO! Caracter '#' Invalido!";
                case '@':
                    return "ERRO! Caracter '@' Invalido!";
                default:
            }
        }
        return saida;
    }

    /*
     ************************************MANIPULAÇÕES DE DATAS - GLOBAL ******************************************************
     */
    /*Método utilizado para verificar se a data é válida*/
    public String dataValidaNascimento(String d) {
        int dia_min = 1, mes_min = 1, mes_max = 12, ano_min = 1958, ano_max = 2000;//inicialização das variaveis para comparação
        String[] data = d.split("/");//separo a data pelas barras 

        if (verificaNumeroDoisDigitos(d) != null) {
            return "ERRO! Data Inválida!";
        }
        if (data.length == 3) {//verifico se a data foi digitada corretamente 
            int dia, mes, ano;

            /*verifica se o dia e o mês possuem dois digitos*/
            String saida = (verificaNumeroDoisDigitos(d));
            if (saida != null) {
                return saida;//se não tiver dois digitos retorna como erro
            }

            if (eNumero(data[0])) {//verifica se o dia é um numero

                dia = Integer.parseInt(data[0]);
            } else {
                return "ERRO! Data Inválida!";//else da verificação se é número
            }
            if (eNumero(data[1])) {//verifica se o més é um numero
                mes = Integer.parseInt(data[1]);
            } else {
                return "ERRO! Data Inválida!";//else da verificação se é número
            }
            if (eNumero(data[2])) {//verifica se o ano é um numero
                ano = Integer.parseInt(data[2]);
            } else {
                return "ERRO! Data Inválida!";//else da verificação se é número
            }
            if (ano >= ano_min && ano <= ano_max) {//Estabelece a idade minima e máxima [18 - 60]
                if (mes >= mes_min && mes <= mes_max) {//Verifica se o més está entre janeiro e dezembro
                    int qtd_dias_mes = getQuantidadeDiasDoMes(mes); //pega a quantidade de dias do més
                    if (dia >= dia_min && dia <= qtd_dias_mes) {
                        return null;
                    } else {
                        return "ERRO! Data Inválida!";//Caso o dia não esteja definido dento do intervalo retorna inválido
                    }
                } else {
                    return "ERRO! Data Inválida!";//Caso o més não esteja definido dento do intervalo retorna inválido
                }
            } else {
                return "ERRO! Data Inválida!";//Caso o dia més esteja definido dento do intervalo retorna inválido
            }
        } else {
            return "ERRO! Data Inválida!";//Caso a entrada não forneça diferente do padrão 00/00/0000 
        }

    }

    /*Verifica se a data de admissão aconteceu depois da data de formatura*/
    public String dataAdmissaoValida(String dt_form, String dt_admis) {

        try {
            if (dataMenorIgualHoje(dt_admis)) {
                try {
                    if (dataMaior(dt_form, dt_admis)) {
                        return null;
                    } else {
                        return "ERRO! Inconsistencia de datas: Formatura posterior a admissão!";//Caso a data de admissão seja menor que a data de formatura 
                    }
                } catch (ParseException ex) {

                    Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {

                return "ERRO! Data fornecida maior que hoje!";
            }
        } catch (ParseException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    /*Verifica se a data de formatura aconteceu após a maior idade*/
    public String dataFormaturaValida(String dt_nasc, String dt_form, String dt_admis) {

        try {
            if (dataMenorIgualHoje(dt_form)) {
                if (dataMaior(Retorna.dataMinimaFormatura(dt_nasc), dt_form)) {

                    /*verifica se a data de formatura e menor que a data de admissão*/
                    String ret = (dataAdmissaoValida(dt_form, dt_admis));
                    if (ret != null) {
                        return ret;//caso a data de formatura seja invalida retorna erro
                    }
                    return null;

                } else {
                    return "Data inválida!";
                }
            } else {
                return "Data fornecida maior que hoje!";
            }
        } catch (ParseException ex) {
            Logger.getLogger(Verifica.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /*verifica se a data da formatura é menor ou igual a hoje*/
    public static boolean dataMenorIgualHoje(String data1) throws ParseException {

        DateFormat formatter;
        Date d1, d2;
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        d1 = formatter.parse(data1);
        d2 = formatter.parse(Retorna.dtAtual());

        if (d2.before(d1)) {
            return false;
        } else if (d2.after(d1)) {
            return true;
        } else {
            return true;
        }
    }

    /*Verifica se o segundo parametro é maior que o primeiro(data2 > data1)*/
    public boolean dataMaior(String data1, String data2) throws ParseException {

        DateFormat formatter;
        Date d1, d2;
        formatter = new SimpleDateFormat("dd/MM/yyyy");
        d1 = formatter.parse(data1);
        d2 = formatter.parse(data2);

        if (d1.before(d2)) {
            return true;
        } else if (d1.after(d2)) {
            return false;
        } else {
            return false;
        }

    }
    /*Método usado para saber quantos dias tem cada més*/

    public static int getQuantidadeDiasDoMes(int mes) {
        Calendar datas = new GregorianCalendar();
        datas.set(Calendar.MONTH, mes - 1);//2
        int quantidadeDias = datas.getActualMaximum(Calendar.DAY_OF_MONTH);
        return quantidadeDias;
    }

    /*
     ************************************MANIPULAÇÕES DE NÚMEROS - GLOBAL ******************************************************
     */
    /*Método usado para verificar se a String é númerica*/
    public static boolean eNumero(String s) {
        char[] c = s.toCharArray();//cria um array de caracter

        for (int i = 0; i < c.length; i++) { // verifica se o char não é um dígito

            if (!Character.isDigit(c[i])) {
                return false;
            }
        }
        return true;
    }

    /*Verifica se os campos que compoe a data tem dois digitos*/
    private String verificaNumeroDoisDigitos(String d) {
        String[] data = d.split("/");//separo a data pelas barras 
        if (!(data[0].length() == 2)) {
            return "ERRO! Data Inválida!";
        } else if (!(data[1].length() == 2)) {
            return "ERRO! Data Inválida!";
        } else {
            return null;
        }
    }

}
