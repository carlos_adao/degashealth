
package Controller;

import Auxiliares.Verifica;
import Models.AuxiliarMedico;
import java.sql.SQLException;

/**
 * Controller usado para realizar as operações na classe Auxiliar Medico que
 * pode ser tanto Auxiliar de Enfermagem ou Técnico de Enfermagem isso é
 * definido pelo tipo
 */
public class AuxiliarMedicoController {

    private String saida;
    private final Verifica verifica;

    public AuxiliarMedicoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoAuxiliarMedico(String nome, String sexo, String COREN, String nacionalidade, String data_nasc, String data_admiss, String data_form, String tipo) throws SQLException {

        //Método usado para verificar de tem caracter invalido no nome;
        if ((saida = verifica.caracterInvalido(nome)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido no CRM;
        if ((saida = verifica.caracterInvalido(COREN)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido na nacionalidade;
        if ((saida = verifica.caracterInvalido(nacionalidade)) != null) {
            return saida;
        }
        //Método usado para verificar se o COREN informado já está cadastrado.
        if ((saida = verifica.corenRepetidoAuxMedico(COREN)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Nascimento.
        if ((verifica.dataValidaNascimento(data_nasc)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Formatura.
        if ((verifica.dataFormaturaValida(data_nasc, data_form, data_admiss)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Admissão.
        if ((verifica.dataAdmissaoValida(data_form, data_admiss)) != null) {
            return saida;
        }
        /*Caso não aconteça nenhum erro cria o objeto do tipo Medico*/
        AuxiliarMedico aux_medico = new AuxiliarMedico(COREN, nome, nacionalidade, sexo, data_nasc, data_admiss, data_form, tipo);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereAuxiliarMedico(aux_medico)) {
            return "Auxiliar/Técnico inserido!";
        } else {
            return "Falha ao inserir Auxiliar/Técnico!";
        }
    }

    /*Método usado para Criar um objeto do tipo Auxiliar Medico*/
    public AuxiliarMedico criaAuxiliarMedico(String nome, String sexo, String COREN, String nacionalidade, String data_nasc, String data_admiss, String data_form, String tipo) {
        return new AuxiliarMedico(COREN, nome, nacionalidade, sexo, data_nasc, data_admiss, data_form, tipo);

    }
    public String retornaAuxiliarMedico(String nome_aux_med) {
        return verifica.getAuxiliarMedicoDB(nome_aux_med);
    }
    /*método usado para executar os updates da classe dos Auxiliares medicos*/
    public String editAuxiliarMedico(String coren, String tipo, String edicao) {
        return verifica.UpdateAuxiliarMedico(coren, tipo, edicao);
    }
}
