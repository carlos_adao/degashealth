/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.MaterialProcedimento;

/**
 *
 *
 */
public class MaterialProcedimentoController {

    private String saida;
    private final Verifica verifica;

    public MaterialProcedimentoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoRelacionamento(String cod_proc, String cod_mat) {

        MaterialProcedimento map = criaMaterialProcedimento(cod_proc, cod_mat);

        /*Verifica Se o o relacionamento foi inserido com sucesso!*/
        if (verifica.insereMaterialProcedimento(map)) {
            return "Material Incluido com sucesso no procedimento!";
        } else {
            return "Falha ao tentar relaciona material - procedimento!";
        }
    }
    /*Cria um objeto de relacionamento*/
    public MaterialProcedimento criaMaterialProcedimento(String cod_proc, String cod_mat) {
        return new MaterialProcedimento(cod_proc, cod_mat);
    }

    /*Retorna uma lista de materiais pertencente ao procedimento*/
    public String listMateriaisProcedimento(String cod_proc) {
        return verifica.getListaMateriaislDB(cod_proc);
    }
}
