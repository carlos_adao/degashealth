/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.Procedimento;

/**
 *
 *
 */
public class ProcedimentoController {

    private String saida;
    private final Verifica verifica;

    public ProcedimentoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoProcedimento(String cod, String desc, String custo) {
        //Método usado para verificar de tem caracter invalido na descrição;
        if ((saida = verifica.caracterInvalido(desc)) != null) {
            return saida;
        }
        //Método usado para verificar se o código do procedimento já está cadastrado
        if ((saida = verifica.codigoProcedimentoRepetido(cod)) != null) {
            return saida;
        }

        if ((saida = verifica.custoValido(custo)) != null) {
            return saida;
        }

        Procedimento proc = criaProcedimento(cod, desc, custo);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereProcedimento(proc)) {
            return "Procedimento Incluido com Sucesso!";
        } else {
            return "Falha ao inserir Procedimento!";
        }
    }

    /*Método usado para Criar um objeto do tipo procedimento*/
    public Procedimento criaProcedimento(String cod, String desc, String custo) {
        float custo_float = Float.parseFloat(custo.replace("R$", "").replace("$", "").replace(",", ".").replace(".", ","));//convert o custo de String para float
        return new Procedimento(cod, desc, custo_float);
    }

    /*método usado para retorna um procedimento de acordo com seu codigo*/
    public String retornaPocedimento(String cod_proc) {
        return verifica.getProcedimentoDB(cod_proc);
    }
    
    /*método usado para executar os updates da classe dos procedimentos*/
    public String editProcedimento(String cod, String tipo, String edicao) {
        return verifica.UpdateProcedimento(cod, tipo, edicao);
    }

}
