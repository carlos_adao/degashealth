/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.EquipamentoProcedimento;

/**
 *
 *
 */
public class EquipamentoProcedimentoController {

    private String saida;
    private final Verifica verifica;

    public EquipamentoProcedimentoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }
    
     public String novoRelacionamento(String cod_proc, String cod_equi) {
        
        EquipamentoProcedimento eqp = criaEquipamentoProcedimento(cod_proc, cod_equi);

        /*Verifica Se o o relacionamento foi inserido com sucesso!*/
        if (verifica.insereEquipamentoProcedimento(eqp)) {
            return "Equipamento Incluido com sucesso no procedimento!";
        } else {
            return "Falha ao tentar relaciona equipamento - procedimento!";
        }
    }
    /*Cria um objeto de relacionamento*/
    public EquipamentoProcedimento criaEquipamentoProcedimento(String cod_proc, String cod_equi) {
        return new EquipamentoProcedimento(cod_proc, cod_equi);
    }
    
    /*Retorna uma lista de equipamentos pertencente ao procedimento*/
    public String listEquipamentosProcedimento(String cod_proc) {
        return verifica.getListaEquipamentoslDB(cod_proc);
    }

}
