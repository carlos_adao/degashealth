/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.Equipamento;

/**
 *
 *
 */
public class EquipamentoController {

    private String saida;
    private final Verifica verifica;

    public EquipamentoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }
    
    public String novoEquipamento(String cod, String desc, String valor){
    
        /*Verifica de tem caracter invalido na descrição*/
        if ((saida = verifica.caracterInvalido(desc)) != null) {
            return saida;
        }
        //Método usado para verificar se o tombo do equipamento ou o nome é repetido
        if ((saida = verifica.tomboEquipamentoRepetido(cod)) != null) {
            return saida;
        }
        
         Equipamento eq = criaEquipamento(cod, desc, valor);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereEquipamento(eq)) {
            return "Equipamento cadastrado com sucesso!";
        } else {
            return "Falha ao inserir Equipamento!";
        }
           
    }
     /*Método usado para Criar um objeto do tipo equipamento*/
    public Equipamento criaEquipamento(String cod, String desc, String valor) {
        float valor_float = Float.parseFloat(valor.replace("R$", "").replace("$", "").replace(",", ".").replace(".", ","));//convert o custo de String para float
        return new Equipamento(cod, desc, valor_float);
    }
    
    /*método usado para editar o tombo do equipamento*/
    public String tombaEquipamento(String cod, String tombo)  {
        return verifica.UpdateTomboEquipamento(cod, tombo);
    }
}
