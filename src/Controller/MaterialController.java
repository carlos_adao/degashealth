/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.Material;

/**
 *
 *
 */
public class MaterialController {

    private String saida;
    private final Verifica verifica;

    public MaterialController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoMaterial(String cod, String desc, String qtd) {
        //Método usado para verificar de tem caracter invalido no nome;
        if ((saida = verifica.caracterInvalido(desc)) != null) {
            return saida;
        }
        //Método usado para verificar se o código do material já está cadastrado
        if ((saida = verifica.codMaterialRepetido(cod)) != null) {
            return saida;
        }

        Material ma = criaMaterial(cod, desc, qtd);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereMaterial(ma)) {
            return "Material Cadastrado com sucesso!";
        } else {
            return "Falha ao inserir Material!";
        }
    }
    /*Método usado para Criar um objeto do tipo material*/

    public Material criaMaterial(String cod, String desc, String qtd) {
        int qtd_int = Integer.parseInt(qtd);
        return new Material(cod, desc, qtd_int);
    }

    /*método usado para retorna um material de acordo com seu codigo*/
    public String retornaMaterial(String cod_ma) {
        return verifica.getMaterialDB(cod_ma);
    }
}
