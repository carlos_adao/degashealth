package Controller;

import Auxiliares.Verifica;
import Models.Medico;
import java.sql.SQLException;

/**
 *
 *
 */
public class MedicoController {

    String saida;
    Verifica verifica;

    public MedicoController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoMedico(String nome, String sexo, String crm, String nacionalidade, String data_nasc, String data_admiss, String data_form) throws SQLException {

        //Método usado para verificar de tem caracter invalido no nome;
        if ((saida = verifica.caracterInvalido(nome)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido no CRM;
        if ((saida = verifica.caracterInvalido(crm)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido na nacionalidade;
        if ((saida = verifica.caracterInvalido(nacionalidade)) != null) {
            return saida;
        }
        //Método usado para verificar se o CRM informado já está cadastrado.
        if ((saida = verifica.crmRepetido(crm)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Nascimento.
        if ((verifica.dataValidaNascimento(data_nasc)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Formatura.
        if ((verifica.dataFormaturaValida(data_nasc, data_form, data_admiss)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Admissão.
        if ((verifica.dataAdmissaoValida(data_form, data_admiss)) != null) {
            return saida;
        }
        /*Caso não aconteça nenhum erro cria o objeto do tipo Medico*/
        Medico medico = criaMedico(nome, sexo, crm, nacionalidade, data_nasc, data_admiss, data_form);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereMedico(medico)) {
            return "Sucesso, Médico inserido!";
        } else {
            return "Falha ao inserir Médico!";
        }
    }

    /*Método usado para Criar um objeto do tipo Médico*/
    public Medico criaMedico(String nome, String sexo, String crm, String nacionalidade, String data_nasc, String data_admiss, String data_form) {
        return new Medico(crm, nome, nacionalidade, sexo, data_nasc, data_admiss, data_form);

    }
    public String retornaMedico(String nome_medico) {
        return verifica.getMedicoDB(nome_medico);
    }

    /*método usado para executar os updates da classe dos medicos*/
    public String editMedico(String crm, String tipo, String edicao) {
        return verifica.UpdateNomeMedico(crm, tipo, edicao);
    }

}
