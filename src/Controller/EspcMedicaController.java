/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Auxiliares.Verifica;
import Models.EspcMedica;

/**
 *
 *
 */
public class EspcMedicaController {

    private String saida;
    private final Verifica verifica;

    public EspcMedicaController() {
        verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novaEspecialidade(String cod, String desc) {

        //Método usado para verificar de tem caracter invalido na descrição;
        if ((saida = verifica.caracterInvalido(desc)) != null) {
            return saida;
        }

        //Método usado para verificar se o código da especialização já esta cadastrado
        if ((saida = verifica.codigoEspecialidadeRepetido(cod)) != null) {
            return saida;
        }

        EspcMedica espec_medica = criaEspecMedica(cod, desc);

        /*Verifica Se a especialidade foi inserida com sucesso!*/
        if (verifica.insereEspecMedica(espec_medica)) {
            return "Especialidade Registrada com sucesso!";
        } else {
            return "Falha ao inserir Especialidade!";
        }

    }
    /*Método usado para Criar um objeto especialidade medica*/
    public EspcMedica criaEspecMedica(String cod, String desc) {
        return new EspcMedica(cod, desc);
    }

    /*método usado para retorna um procedimento de acordo com seu codigo*/
    public String retornaEspecMedica(String cod_espec_medica) {
        return verifica.getEspecMedicaDB(cod_espec_medica);
    }

    /*método usado para executar os updates da classe dos procedimentos*/
    public String editProcedimento(String cod, String tipo, String edicao) {
        return verifica.UpdateProcedimento(cod, tipo, edicao);
    }

}
