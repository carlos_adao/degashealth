package Controller;

import Auxiliares.Verifica;
import Models.Enfermeiro;
import java.sql.SQLException;

public class EnfermeiroController {

    private String saida;
    private final Verifica verifica;

    public EnfermeiroController() {
                verifica = new Verifica();//classe usada para fazer as verificações 
    }

    public String novoEnfermeiro(String nome, String sexo, String COREN, String nacionalidade, String data_nasc, String data_admiss, String data_form) throws SQLException {

        //Método usado para verificar de tem caracter invalido no nome;
        if ((saida = verifica.caracterInvalido(nome)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido no CRM;
        if ((saida = verifica.caracterInvalido(COREN)) != null) {
            return saida;
        }
        //Método usado para verificar de tem caracter invalido na nacionalidade;
        if ((saida = verifica.caracterInvalido(nacionalidade)) != null) {
            return saida;
        }
        //Método usado para verificar se o COREN informado já está cadastrado.
        if ((saida = verifica.corenRepetido(COREN)) != null) {           
            return saida;
        }
        //Método usado para válidar a data de Nascimento.
        if ((verifica.dataValidaNascimento(data_nasc)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Formatura.
        if ((verifica.dataFormaturaValida(data_nasc, data_form, data_admiss)) != null) {
            return saida;
        }
        //Método usado para válidar a data de Admissão.
        if ((verifica.dataAdmissaoValida(data_form, data_admiss)) != null) {
            return saida;
        }
        /*Caso não aconteça nenhum erro cria o objeto do tipo Medico*/
        Enfermeiro enfermeiro = criaEnfermeiro(nome, sexo, COREN, nacionalidade, data_nasc, data_admiss, data_form);

        /*Verifica Se o Médico foi inserido com sucesso!*/
        if (verifica.insereEnfermeiro(enfermeiro)) {
            return "Sucesso, Enfermeiro inserido!";
        } else {
            return "Falha ao inserir Enfermeiro!";
        }
    }

    /*Método usado para Criar um objeto do tipo Médico*/
    public Enfermeiro criaEnfermeiro(String nome, String sexo, String COREN, String nacionalidade, String data_nasc, String data_admiss, String data_form) {
        return new Enfermeiro(COREN, nome, nacionalidade, sexo, data_nasc, data_admiss, data_form);

    }
    public String retornaEnfermeiro(String nome_enfermeiro) {
        return verifica.getEnfermeiroDB(nome_enfermeiro);
    }
    /*método usado para executar os updates da classe dos enfermeiros*/
    public String editEnfermeiro(String coren, String tipo, String edicao) {
        return verifica.UpdateEnfermeiro(coren, tipo, edicao);
    }
}
